package es.ifdsmc.chat;

public class ChatChannel {

	public String nameChannel = "";
	public String ownerChannel = "";
	
	public ChatChannel(String name, String owner) {
		
		this.nameChannel = name;
		this.ownerChannel = owner;
		
	}
	
}
