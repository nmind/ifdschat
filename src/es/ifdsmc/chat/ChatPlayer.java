package es.ifdsmc.chat;

public class ChatPlayer {

	public String namePlayer = "";
	public String channelPlayer = "";
	public String channelActive = "";
	public String idPlayer = "";
	
	public ChatPlayer(String name, String channel, String active) {
		
		this.namePlayer = name;
		this.channelPlayer = channel;
		this.channelActive = active;
		
	}
	
}
