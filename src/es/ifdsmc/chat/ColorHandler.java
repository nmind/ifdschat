/*******************************************************************************
 * Copyright 2015 MaTaXeToS (Pere Gomis)
 *  
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 ******************************************************************************/

package es.ifdsmc.chat;

import org.bukkit.ChatColor;

public class ColorHandler {

	IFDSChat plugin;
	
	public ColorHandler(IFDSChat plugin) {
		
		this.plugin = plugin;
		
	}
	
	public String format(String string) {
		
		String newString = string; 
		
		newString = newString.replaceAll("&0","" + ChatColor.BLACK);
		newString = newString.replaceAll("&1","" + ChatColor.DARK_BLUE);
		newString = newString.replaceAll("&2","" + ChatColor.DARK_GREEN);
		newString = newString.replaceAll("&3","" + ChatColor.DARK_AQUA);
		newString = newString.replaceAll("&4","" + ChatColor.DARK_RED);
		newString = newString.replaceAll("&5","" + ChatColor.DARK_PURPLE);
		newString = newString.replaceAll("&6","" + ChatColor.GOLD);
		newString = newString.replaceAll("&7","" + ChatColor.GRAY);
		newString = newString.replaceAll("&8","" + ChatColor.DARK_GRAY);
		newString = newString.replaceAll("&9","" + ChatColor.BLUE);
		newString = newString.replaceAll("&a","" + ChatColor.GREEN);
		newString = newString.replaceAll("&b","" + ChatColor.AQUA);
		newString = newString.replaceAll("&c","" + ChatColor.RED);
		newString = newString.replaceAll("&d","" + ChatColor.LIGHT_PURPLE);
		newString = newString.replaceAll("&e","" + ChatColor.YELLOW);
		newString = newString.replaceAll("&f","" + ChatColor.WHITE);
		
		return newString;
	}
	
}
