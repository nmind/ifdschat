/*******************************************************************************
 * Copyright 2015 MaTaXeToS (Pere Gomis)
 *  
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 ******************************************************************************/

package es.ifdsmc.chat;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import es.ifdsmc.database.SQLite;
import es.ifdsmc.listeners.ChatCommands;
import es.ifdsmc.listeners.ClientCommands;
import es.ifdsmc.listeners.AdminChatCommand;
import es.ifdsmc.listeners.PlayerListener;
import es.ifdsmc.utils.Utils;

public class IFDSChat extends JavaPlugin implements Listener {
	
	public HashMap<String, ChatPlayer> players = new HashMap<String, ChatPlayer>();
	public HashMap<String, ChatChannel> channels = new HashMap<String, ChatChannel>();
	
	public SQLite SQLite;
	public Connection c = null;
	
	public ColorHandler ColorHandler = new ColorHandler(this);
	public Utils Utils = new Utils(this);
	public ClientCommands clientCommands = new ClientCommands(this);
	
	public String __VERSION__ = "IFDS Chat v2.1";
	
	public Boolean __DEBUG__ = false;
	
	
	@Override
	public void onEnable() {
		
		PluginManager pm = getServer().getPluginManager();
		
		//Registramos los eventos
		pm.registerEvents(new PlayerListener(this), this);
		
		SQLite = new SQLite(this, "data.db");
    	
    	try {
			c = SQLite.openConnection();
			
	        //Seteamos los comandos
	        getCommand("c").setExecutor(new ClientCommands(this));
	        getCommand("a").setExecutor(new AdminChatCommand(this));
	        getCommand("cm").setExecutor(new ChatCommands(this));
	        //getCommand("ca").setExecutor(new ClientCommands(this));
	        
	        
	        //Obtenemos los canales de la base de datos
			Statement statementTotal = c.createStatement();
			String sqlTotal = "SELECT COUNT(id) as totalRows FROM chat_channels";
			ResultSet resTotal = statementTotal.executeQuery(sqlTotal);
			resTotal.next();
			
			if(resTotal.getInt("totalRows") > 0) {
				
				int totalChannels = resTotal.getInt("totalRows");

				Statement statement = c.createStatement();
				String sql = "SELECT * FROM chat_channels";
				ResultSet res = statement.executeQuery(sql);
				res.next();
				
				for (int i=0; i<=totalChannels;i++) {
					//Creamos el ChatPlayer con la información default
					
					String channelName = res.getString("name");
					String channelOwner = res.getString("owner");
					
					ChatChannel ChatChannel = new ChatChannel(channelName, channelOwner);
					
					this.channels.put(channelName, ChatChannel);
					
					if (i < totalChannels) {
						res.next();
					}
				}
				getLogger().info("IFDSChat Se han cargado un total de " + totalChannels + " canales.");
				
			}
	        
	        getLogger().info("IFDSChat cargado correctamente");
			
		} catch (ClassNotFoundException e) {
			getLogger().info("IFDSChat ERROR AL CARGAR MYSQL");
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
			getLogger().info("IFDSChat ERROR AL CARGAR MYSQL");
		}        
	}
	
	public void debug(Player p, String string) {
		if (__DEBUG__) {
			p.sendMessage(string);	
		}
	}
	
	@Override
	public void onDisable() {
		
		Utils.saveAllData();
		
		getLogger().info("IFDSChat descargado correctamente");
	}
	
}
