/*******************************************************************************
 * Copyright 2015 MaTaXeToS (Pere Gomis)
 *  
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 ******************************************************************************/

package es.ifdsmc.listeners;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;
import es.ifdsmc.chat.ChatPlayer;
import es.ifdsmc.chat.IFDSChat;

public class AdminChatCommand implements CommandExecutor {

	IFDSChat plugin;
	
	public AdminChatCommand(IFDSChat plugin) {
		this.plugin = plugin;
	}

	@Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
    	
        String gray = ChatColor.GRAY.toString();
        String gold = ChatColor.GOLD.toString();
    	String prefixPLUGIN = gold + "[IFDS Chat] " + gray;
    	
		//Si es un jugador quien ha ejecutado el comando
		if (sender instanceof Player) {
			
			//Obtenemos el jugador del comando
			Player player = (Player) sender;
			
			//Obtenemos el jugador en el PermissionsEX
			PermissionUser playerPEX = PermissionsEx.getUser(player);
			
			if (playerPEX.has("ifds.chat.admin")) {
				
				//Obtenemos el permiso
				String prefix = playerPEX.getPrefix();
				
				//Obtenemos el canal del jugador
				ChatPlayer ChatPlayer = plugin.players.get(player.getName());
				String PlayerChannel = ChatPlayer.channelPlayer;
				String prefix_channel = "";

				//Si el canal del usuario no esta vacio
				if (PlayerChannel != null && !PlayerChannel.isEmpty() && !PlayerChannel.equalsIgnoreCase("none")) {
					//Creamos el prefijo del canal
					prefix_channel = "" + ChatColor.GRAY + "[" + ChatColor.AQUA + PlayerChannel + ChatColor.GRAY + "]";
				}
					
				//Color de OP en el nick
				String PlayerName = player.getName();
				if (player.isOp()) {
					PlayerName = ChatColor.DARK_RED.toString() + PlayerName;
				}
							
				//Le damos formato
				String prefix_formatted = plugin.ColorHandler.format(prefix);
					
				//Construimos el mensaje
				String message = "";
				
				if (args.length >= 1) {
					for (int i=0;i<args.length;i++) {
						if (message.length() > 0) {
							message = message + " ";
						}
						message = message + args[i]; 
					}
						
					@SuppressWarnings("deprecation")
					Player[] onlinePlayers = Bukkit.getOnlinePlayers();
					for (int i = 0; i < onlinePlayers.length; i++) {
						Player other = onlinePlayers[i];

						PermissionUser rp = PermissionsEx.getUser(other);
						
						if (rp.has("ifds.chat.admin")) {
							other.sendMessage(prefix_channel + prefix_formatted + PlayerName + ChatColor.WHITE + ": " + ChatColor.RED + message);
						}
					}
					return true;
					
				} else {
					player.sendMessage(prefixPLUGIN + "Debes especificar un mensaje.");
				}
			} else {
				player.sendMessage(prefixPLUGIN + "Para usar este comando tienes que ser administrador.");
			}
		}
    	return false;
	}
	
}
