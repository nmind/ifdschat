/*******************************************************************************
 * Copyright 2015 MaTaXeToS (Pere Gomis)
 *  
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 ******************************************************************************/

package es.ifdsmc.listeners;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import es.ifdsmc.chat.ChatChannel;
import es.ifdsmc.chat.ChatPlayer;
import es.ifdsmc.chat.IFDSChat;
import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;

public class ClientCommands implements CommandExecutor {

	private IFDSChat plugin;

    String cyan = ChatColor.DARK_AQUA.toString();
    String white = ChatColor.WHITE.toString();
    String gray = ChatColor.GRAY.toString();
    String red = ChatColor.RED.toString();
    String gold = ChatColor.GOLD.toString();
    String yellow = ChatColor.YELLOW.toString();
    String dgray = ChatColor.DARK_GRAY.toString();
    String green = ChatColor.GREEN.toString();
	String prefix = gold + "[IFDS Chat] " + gray;
	
	public ClientCommands(IFDSChat plugin) {
	
		this.plugin = plugin;
		
	}
	
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
    	
		//Si es un jugador quien ha ejecutado el comando
		if (sender instanceof Player) {
			
			//Obtenemos el jugador del comando
			Player player = (Player) sender;
			
			//Obtenemos el jugador en el PermissionsEX
			PermissionUser playerPEX = PermissionsEx.getUser(player);
			
			if (playerPEX.has("ifds.chat")) {
				if (args.length <= 0) {
					showHelp(player);
				} else {
					
					String command = args[0];
					
					if (args.length >= 1) {
						switch(command) {
							case "help":
								showHelp(player);
							break;
							case "version":
								showVersion(player);
							break;
							case "create":
								if (args.length >= 2) {
									try {
										createChannel(player,args[1]);
									} catch (SQLException e) {
										player.sendMessage(prefix + "Se ha producido un error al crear el canal.");
										player.sendMessage(e.getMessage());
									}
								} else {
									player.sendMessage(prefix + "Faltan argumentos, usa /c create <nombre_del_canal>");
								}
							break;
							case "delete":
								try {
									deleteChannel(player);
								} catch (SQLException e) {
									player.sendMessage(prefix + "Se ha producido un error borrar el canal.");
									player.sendMessage(e.getMessage());
								}
							break;
							case "list":
								try {
									listChannels(player);
								} catch (SQLException e) {
									player.sendMessage(prefix + "Se ha producido un error borrar el canal.");
									player.sendMessage(e.getMessage());
								}
							break;
							case "add":
								if (args.length >= 2) {
									try {
										addPlayerChannel(player,args[1]);
									} catch (SQLException e) {
										player.sendMessage(prefix + "Se ha producido un error al agregar el usuario al canal.");
										player.sendMessage(e.getMessage());
									}
								} else {
									player.sendMessage(prefix + "Faltan argumentos, usa /c add <usuario>");
								}
							break;
							case "leave":
								try {
									leaveChannel(player);
								} catch (SQLException e) {
									player.sendMessage(prefix + "Se ha producido un error salir el canal.");
									player.sendMessage(e.getMessage());
								}
							break;		
							case "focus":
							case "f":
								if (args.length >= 2) {
									try {
										focusChannel(player,args[1]);
									} catch (SQLException e) {
										player.sendMessage(prefix + "Se ha producido un error al hacer focus en el canal.");
										player.sendMessage(e.getMessage());
									}
								} else {
									try {
										focusChannel(player);
									} catch (SQLException e) {
										player.sendMessage(prefix + "Se ha producido un error al hacer focus en el canal.");
										player.sendMessage(e.getMessage());
									}
								}
							break;
						}
					} else {
						player.sendMessage(prefix + "Faltan argumentos, usa /c help para m�s ayuda");
					}
				}
				return true;
			}
		}
    	return false;
    }
    
    private void createChannel(Player player, String channelName) throws SQLException {
    	
		//Obtenemos el ChatPlayer del jugador que ha creado el canal
		ChatPlayer ChatPlayer = plugin.players.get(player.getName());
    	
		if (ChatPlayer.channelPlayer.equalsIgnoreCase("none")) {
			Statement statement = plugin.c.createStatement();
			String sql = "SELECT *, COUNT(id) as totalRows FROM chat_channels WHERE name = '" + channelName + "';";
			ResultSet res = statement.executeQuery(sql);
			res.next();
			
			ChatChannel ChatChannel;
			
			if(res.getInt("totalRows") == 0) {
				
				//No existe el canal, asi que lo crearemos
				Statement statementInsert = plugin.c.createStatement();
				statementInsert.executeUpdate("INSERT INTO chat_channels ('name', 'owner') VALUES ('" + channelName + "', '" + player.getName() + "');");
				
				//Actualizamos su canal al canal creado
				ChatPlayer.channelPlayer = channelName;
				
				//Creamos el ChatChannel con el nombre del canal y el creador
				ChatChannel = new ChatChannel(channelName, player.getName());
				
				//Agregamos el jugador a lista de players con el ChatPlayer
				plugin.channels.put(channelName, ChatChannel);
				
				player.sendMessage(prefix + "Se ha creado el canal " + channelName + " correctamente.");
			} else {
				//No podemos crear el canal, ya existe uno con este nombre
				player.sendMessage(prefix + "Ya existe un canal con este nombre.");	
			}
		} else {
			player.sendMessage(prefix + "Ya perteneces a un canal.");
		}
    }
    
    private void deleteChannel(Player player) throws SQLException {
    	
		//Obtenemos el ChatPlayer del jugador que ha creado el canal
		ChatPlayer ChatPlayer = plugin.players.get(player.getName());
    	
		if (ChatPlayer.channelPlayer.equalsIgnoreCase("none")) {
			player.sendMessage(prefix + "Para ejecutar este comando, debes pertenecer a un canal.");
		} else {
			
			String channelName = ChatPlayer.channelPlayer;
			
			ChatChannel ChatChannel = plugin.channels.get(channelName);
			
			//Si somos el creador del canal
			if (ChatChannel.ownerChannel.equalsIgnoreCase(player.getName())) {
				
				//Eliminamos el canal de la base de datos
				Statement statementDelete = plugin.c.createStatement();
				statementDelete.executeUpdate("DELETE FROM chat_channels WHERE name = '" + channelName + "';");
				
				//Eliminamos el canal de la lista de canales
				plugin.channels.remove(channelName);
				
				@SuppressWarnings("deprecation")
				Player[] onlinePlayers = Bukkit.getOnlinePlayers();
				for (int i = 0; i < onlinePlayers.length; i++) {
					Player other = onlinePlayers[i];
					//Obtenemos el ChatPlayer del jugador
					ChatPlayer ChatPlayerOther = plugin.players.get(other.getName());
					
					if (ChatPlayerOther.channelPlayer.equalsIgnoreCase(channelName)) {
						ChatPlayerOther.channelPlayer = "none";
						ChatPlayerOther.channelActive = "local";
					}
				}
				player.sendMessage(prefix + "Se ha eliminado el canal correctamente.");
				
			} else {
				player.sendMessage(prefix + "Solo el due�o puede eliminar el canal.");	
			}
		}
    }
    
    private void leaveChannel(Player player) throws SQLException {
    	
		//Obtenemos el ChatPlayer del jugador que ha creado el canal
		ChatPlayer ChatPlayer = plugin.players.get(player.getName());
    	
		if (ChatPlayer.channelPlayer.equalsIgnoreCase("none")) {
			player.sendMessage(prefix + "Para ejecutar este comando, debes pertenecer a un canal.");
		} else {
			
			String channelName = ChatPlayer.channelPlayer;
			
			ChatChannel ChatChannel = plugin.channels.get(channelName);
			
			//Si NO somos el creador del canal
			if (!ChatChannel.ownerChannel.equalsIgnoreCase(player.getName())) {
				//Actualizamos los datos del usuario quitandole el canal
				ChatPlayer.channelPlayer = "none";
				ChatPlayer.channelActive = "local";
				
				player.sendMessage(prefix + "Te has salido del canal correctamente.");
				
			} else {
				player.sendMessage(prefix + "Utiliza /c delete para salirte del canal siendo el due�o.");	
			}
		}
    }
    
    private void addPlayerChannel(Player player, String playerToAdd) throws SQLException {
    	
		//Obtenemos el ChatPlayer
		ChatPlayer ChatPlayer = plugin.players.get(player.getName());
    	
		if (ChatPlayer.channelPlayer.equalsIgnoreCase("none")) {
			player.sendMessage(prefix + "Para ejecutar este comando, debes pertenecer a un canal.");
		} else {
			
			String channelName = ChatPlayer.channelPlayer;
			
			ChatChannel ChatChannel = plugin.channels.get(channelName);
			
			//Si somos el creador del canal
			if (ChatChannel.ownerChannel.equalsIgnoreCase(player.getName())) {

				//Obtenemos el ChatPlayer del jugador que ha creado el canal
				ChatPlayer OtherChatPlayer = plugin.players.get(playerToAdd);
				
				if (OtherChatPlayer == null) {
					player.sendMessage(prefix + "No se ha encontrado ningun jugador con ese nombre.");
				} else {
					//Actualizamos los datos del usuario agregandole el canal
					OtherChatPlayer.channelPlayer = channelName;
					
					player.sendMessage(prefix + "Se ha agregado correctamente el usuario al canal.");
				}
			} else {
				player.sendMessage(prefix + "Solo el due�o puede agregar usuarios al canal.");	
			}
		}
    }
    
    private void changeChannel(ChatPlayer ChatPlayer, Player player, String channel) throws SQLException {
		//Cambiamos el canal activo
		ChatPlayer.channelActive = channel;
		
		player.sendMessage(prefix + "Se ha cambiado el foco del canal a " + channel + ".");
    }
    
    private void focusChannel(Player player, String channel) throws SQLException {

		//Obtenemos el jugador en el PermissionsEX
		PermissionUser pp = PermissionsEx.getUser(player);
		
		ChatPlayer ChatPlayer = plugin.players.get(player.getName());
		
		if (channel.equalsIgnoreCase("admin")) {
			if (!pp.has("ifds.chat.admin")) {
				player.sendMessage(prefix + "Necesitas ser un admin para hacer focus a este canal.");	
			} else {
				if (ChatPlayer.channelActive.equalsIgnoreCase("admin")) {
					player.sendMessage(prefix + "Ya estas en el canal ADMIN.");
				} else {
					changeChannel(ChatPlayer, player, channel);
				}
			}
		} else if (channel.equalsIgnoreCase("local")) {
			if (ChatPlayer.channelActive.equalsIgnoreCase("local")) {
				player.sendMessage(prefix + "Ya estas en el canal LOCAL.");
			} else {
				changeChannel(ChatPlayer, player, channel);
			}
		} else if (channel.equalsIgnoreCase("global")) {
			if (ChatPlayer.channelActive.equalsIgnoreCase("global")) {
				player.sendMessage(prefix + "Ya estas en el canal GLOBAL.");
			} else {
				changeChannel(ChatPlayer, player, channel);
			}
		} else {
			player.sendMessage(prefix + "Solo puedes hacer focus en los canales: local/global/admin");
			player.sendMessage(prefix + "Si quieres hacer focus al canal privado, escribe: /c focus");
		}
		
    }
    
    private void focusChannel(Player player) throws SQLException {
		ChatPlayer ChatPlayer = plugin.players.get(player.getName());
    	
		if (ChatPlayer.channelPlayer.equalsIgnoreCase("none")) {
			player.sendMessage(prefix + "Para ejecutar este comando, debes pertenecer a un canal.");
		} else {
			
			String channelName = ChatPlayer.channelPlayer;
			
			if (ChatPlayer.channelActive.equalsIgnoreCase("local")) {
				//Cambiamos el canal activo
				ChatPlayer.channelActive = channelName;
				
				player.sendMessage(prefix + "Se ha cambiado el foco del canal a " + channelName + ".");
			} else {
				//Cambiamos el canal activo
				ChatPlayer.channelActive = "local";
				
				player.sendMessage(prefix + "Se ha cambiado el foco del canal a Local/Global.");
			}	
		}
    }
    
    private void listChannels(Player player) throws SQLException {
    	
    	player.sendMessage(prefix + "Lista de canales");
    	
    	if (plugin.channels.isEmpty()) {
    		player.sendMessage(prefix + "No hay ningun canal disponible");	
    	} else {
	    	for(Entry<String, ChatChannel> entry : plugin.channels.entrySet()) {
	    	    String channelName = entry.getKey();
	    	    ChatChannel ChatChannel = entry.getValue();
	
	    	    player.sendMessage(prefix + channelName + " - Owner: " + ChatChannel.ownerChannel);
	    	}		
    	}
    }
    
    private void showVersion(Player player) {
    	player.sendMessage(prefix + plugin.__VERSION__ + white + " By MaTaXeToS");
    }
    
    
    private void showHelp(Player player) {
    	player.sendMessage(prefix + white + "Lista de comandos:");
    	player.sendMessage(prefix + "/c create <nombre_del_canal> " + white + "- Crea un canal con el nombre especificado");
    	player.sendMessage(prefix + "/c delete " + white + "- Elimina el canal");
    	player.sendMessage(prefix + "/c add <usuario> " + white + "- Agregas un usuario al canal");
    	//player.sendMessage(prefix + "/c kick <usuario> - Kickeas un usuario del canal");
    	player.sendMessage(prefix + "/c leave " + white + "- Sales del canal");
    	player.sendMessage(prefix + "/c focus " + white + "- Cambias el foco del canal (Privado - Local)");
    	player.sendMessage(prefix + "/c focus local/global/admin" + white + "- Cambias el foco del canal (Local,Global,Admin)");
    	player.sendMessage(prefix + "/c list " + white + "- Muestra una lista de los canales disponibles");
    	player.sendMessage(prefix + "/c version " + white + "- Muestra la versi�n del plugin");
    	
    	player.sendMessage(prefix + "/cm <mensaje> " + white + "- Envia un mensaje al canal privado sin hacer focus");
    }
}
