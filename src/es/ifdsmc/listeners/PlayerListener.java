/*******************************************************************************
 * Copyright 2015 MaTaXeToS (Pere Gomis)
 *  
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 ******************************************************************************/

package es.ifdsmc.listeners;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerKickEvent;

import es.ifdsmc.chat.ChatPlayer;
import es.ifdsmc.chat.IFDSChat;
import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;

public class PlayerListener implements Listener {

	private final IFDSChat plugin;
	
	public PlayerListener(IFDSChat plugin) {
		this.plugin = plugin;
	}
	
	//Cuando un usuario se conecta
	@EventHandler
	public void onPlayerConnect(PlayerJoinEvent e) throws SQLException {
		Player p = e.getPlayer();
		
		//Ya tenemos el usuario en los datos internos del servidor
		if (plugin.players.get(p.getName()) == null) {
		
			Statement statement = plugin.c.createStatement();
			String sql = "SELECT *, COUNT(id) as totalRows FROM chat_players WHERE name = '" + p.getName() + "';";
			ResultSet res = statement.executeQuery(sql);
			res.next();
			
			ChatPlayer ChatPlayer;
			
			if(res.getInt("totalRows") == 0) {
				//No existe el usuario en la base de datos
				//Creamos el registro
				Statement statementInsert = plugin.c.createStatement();
				String sqlInsert = "INSERT INTO chat_players ('name', 'channel', 'active') VALUES ('" + p.getName() + "', 'none', 'local');";
				//statementInsert.executeUpdate("INSERT INTO chat_players SET name = '" + p.getDisplayName() + "', channel='none', active='local';");
				statementInsert.executeUpdate(sqlInsert);
				
				//Creamos el ChatPlayer con la información default
				ChatPlayer = new ChatPlayer(p.getName(), "none", "local");
			} else {
				//Creamos el ChatPlayer con la información de la base de datos
				ChatPlayer = new ChatPlayer(p.getName(), res.getString("channel"), res.getString("active"));
			}
			
			//Agregamos el jugador a lista de players con el ChatPlayer
			plugin.players.put(p.getName(), ChatPlayer);
		}
	}
	
    private Boolean outOfRange(Location l, Location ll) {
        if (l.equals(ll)) {
            return false;
        } else if (l.getWorld() != ll.getWorld()) {
            return true;
        }
        return l.distanceSquared(ll) > 1000;
    }
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onAChatPlayer(AsyncPlayerChatEvent e) {
		
		//Cancelamos el evento del chat, asi no se muestra el chat default
		//e.setCancelled(true);
		
		//Obtenemos el jugador del evento
		Player p = e.getPlayer();
		
		plugin.debug(p, "Escribimos: " + e.getMessage());
		
		//Obtenemos el jugador en el PermissionsEX
		PermissionUser pp = PermissionsEx.getUser(p);
		
		//Obtenemos el mensaje
		String message = e.getMessage();
		String messageWithExclamation = message.substring(1);
		
		//Obtenemos el permiso
		String prefix = pp.getPrefix();
		
		//Le damos formato
		String prefix_formatted = plugin.ColorHandler.format(prefix);
		
		//Obtenemos el canal del jugador
		ChatPlayer ChatPlayer = plugin.players.get(p.getName());
		String PlayerChannel = ChatPlayer.channelPlayer;
		String prefix_channel = "";
		
		//Si el canal del usuario no esta vacio
		if (PlayerChannel != null && !PlayerChannel.isEmpty()) {
			if (!ChatPlayer.channelPlayer.equalsIgnoreCase("none")) {
				//Creamos el prefijo del canal
				
				plugin.debug(p, "TENEMOS UN CANAL: " + PlayerChannel);
				
				prefix_channel = "" + ChatColor.GRAY + "[" + ChatColor.AQUA + PlayerChannel + ChatColor.GRAY + "]";
			}
		}
		
		//Seteamos el formato del mensaje
		String MessageFormat = "<channel><group-prefix><player>: <message>";
		MessageFormat = MessageFormat.replace("<channel>", prefix_channel);
		MessageFormat = MessageFormat.replace("<player>", "%1$s");
		MessageFormat = MessageFormat.replace("<group-prefix>", prefix_formatted); //something like that
		MessageFormat = MessageFormat.replace("<message>", "%2$s");
		e.setFormat(MessageFormat);
		
		if (message.startsWith("!")) {
			plugin.debug(p, "Enviamos mensaje global");
			
			e.setMessage(ChatColor.WHITE + messageWithExclamation);
			
			if (messageWithExclamation.length() <= 0) {
	            e.getPlayer().sendMessage("Tienes que escribir un mensaje");
	            e.setCancelled(true);
			}
		} else {
			if (ChatPlayer.channelActive.equalsIgnoreCase("global")) {
			
				plugin.debug(p, "Tenemos seteado el canal GLOBAL");
				e.setMessage(ChatColor.WHITE + message);
		        
			} else if (ChatPlayer.channelActive.equalsIgnoreCase("local")) {
				
				plugin.debug(p, "Tenemos seteado el canal LOCAL");
				e.setMessage(ChatColor.GRAY + message); 
				
				//Eliminamos los usuarios que no esten cerca nuestro
		        for (Player r : new HashSet<Player>(e.getRecipients())) {
		            if (outOfRange(e.getPlayer().getLocation(), r.getLocation())) {
		                e.getRecipients().remove(r);
		            }
		        }
				
		        if (e.getRecipients().size() == 1) {
		            e.getPlayer().sendMessage("No hay nadie cerca...");
		            e.setCancelled(true);
		        }
			}  else if (ChatPlayer.channelActive.equalsIgnoreCase("admin")) {
				
				plugin.debug(p, "Tenemos seteado el canal ADMIN");
				
		        for (Player r : new HashSet<Player>(e.getRecipients())) {
		        	
		    		//Obtenemos el jugador en el PermissionsEX
		    		PermissionUser rp = PermissionsEx.getUser(r);
		        	
		        	if (!rp.has("ifds.chat.admin")) {
		        		e.getRecipients().remove(r);
		        	}
		        }
				
				e.setMessage(ChatColor.RED + message);
		        
			} else {
			
				plugin.debug(p, "Tenemos seteado el canal: " + ChatPlayer.channelActive);
				
				e.setMessage(ChatColor.AQUA + message); 
				
				//Eliminamos los usuarios que no sean de nuestro canal
		        for (Player r : new HashSet<Player>(e.getRecipients())) {
		        	ChatPlayer ChatPlayerOther = plugin.players.get(r.getName());
					if (!ChatPlayer.channelPlayer.equalsIgnoreCase("none")) {
						//Si es el mismo canal que el mio, enviamos el mensaje
						if (!ChatPlayer.channelPlayer.equalsIgnoreCase(ChatPlayerOther.channelPlayer)) {
							e.getRecipients().remove(r);
						}
					}
		        }
			}
		}
	}
	
}
