package es.ifdsmc.utils;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map.Entry;

import es.ifdsmc.chat.ChatPlayer;
import es.ifdsmc.chat.IFDSChat;

public class Utils {

	IFDSChat plugin;
	
	public Utils(IFDSChat plugin) {
		this.plugin = plugin;
	}
	
	
	public void saveAllData() {
		
		int totalUpdates = 0;
		
    	for(Entry<String, ChatPlayer> entry : plugin.players.entrySet()) {
    	    String playerName = entry.getKey();
    	    ChatPlayer ChatPlayer = entry.getValue();

    		Statement statementUpdate;
			try {
				totalUpdates++;
				
				statementUpdate = plugin.c.createStatement();
				statementUpdate.executeUpdate("UPDATE chat_players SET active = '"+ChatPlayer.channelActive+"', channel = '"+ChatPlayer.channelPlayer+"' WHERE name = '" + ChatPlayer.namePlayer + "';");
			} catch (SQLException e) {
				e.printStackTrace();
			}
    	}
    	
    	plugin.getLogger().info("Se han guardado un total de " + totalUpdates + " jugadores.");
    	
    	
	}
	
}
